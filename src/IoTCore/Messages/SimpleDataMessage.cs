﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IoTCore.Messages
{
    public class SimpleDataMessage
    {

        public SimpleDataMessage(string id, MessageTypeEnum type, double rawValue)
        {
            DeviceID = id;
            SesnorType = type;
            RawValue = rawValue;

        }        

        public string DeviceID { get; set; }

        public MessageTypeEnum SesnorType { get; set; }

        public double RawValue { get; set; }
    }
}
