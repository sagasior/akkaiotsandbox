﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IoTCore.Messages
{
    public class ErrorMessage
    {

        public ErrorMessage(string str, string messageID)
        {
            DisplayString = str;
            MessageID = messageID;
        }

        public string MessageID { get; set; }

        public string DisplayString { get; set; }

    }
}
