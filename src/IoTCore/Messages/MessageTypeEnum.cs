﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IoTCore
{
    public enum MessageTypeEnum
    {
        Temperature,
        WindSpeed,
        Direction,
        GroudSpeed,
        PassengerCount,
        WingVibration,
        FuelLevel,
        YawAngle,
        PitchAngle,
        RollAngle,
        EngineRPM,
        WheelPSI,
        Misc1,
        Misc2,
        Misc3
    }
}
