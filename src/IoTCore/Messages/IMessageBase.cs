﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IoTCore.Messages
{
    public interface IMessageBase
    {
        string DeviceID { get; }

        Dictionary<MessageTypeEnum, double> GetSensorValues();
    }
}
