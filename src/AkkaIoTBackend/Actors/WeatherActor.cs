﻿using Akka.Actor;
using IoTCore;
using IoTCore.Messages;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace AkkaIoTBackend.Actors
{

    internal class TemperatureMessage
    {

        public TemperatureMessage(string id, double temp)
        {
            ID = id;
            Temperature = temp;
        }

        public string ID { get; set; }

        public double Temperature { get; set; }
    }


    internal class TemperatureErrorMessage
    {
        public TemperatureErrorMessage(string id, double temp)
        {
            ID = id;
            Temperature = temp;
        }

        public string ID { get; set; }

        public double Temperature { get; set; }
    }

    public class WeatherActor : ReceiveActor
    {

        private double temp;
        private IActorRef _consoleActor;

        public const string Name = "WeatherActor";

        public WeatherActor()
        {
            _consoleActor = Context.ActorSelection(ActorConfig.ConsoleActorConfig.Path).ResolveOne(new TimeSpan(0, 0, 5)).Result; ;
            Initalize();
        }

        private void Initalize()
        {          

            Receive<TemperatureMessage>(msg =>
            {
                if (msg.ID == null)
                    return;

                if(msg.Temperature >= 50)
                {
                    //Dispatch a new message to myself inside the error messagebox
                    Self.Tell(new TemperatureErrorMessage(msg.ID, msg.Temperature));
                    return;
                }

                if(msg.Temperature < 50)
                {
                    //alert console
                    _consoleActor.Tell(new ConsoleWriteMessage(String.Format("Temperture is {0} from ID {1}", msg.Temperature, msg.ID)));
                    return;
                }                

                if(temp - msg.Temperature > 15)
                {
                    Thread.Sleep(10);
                    _consoleActor.Tell(new ConsoleWriteMessage(String.Format("Temperture dela was big so I slept for 10ms from ID {1}", msg.Temperature, msg.ID)));
                }

                temp = msg.Temperature;

            });

            Receive<TemperatureErrorMessage>(msg =>
            {
                //Makes a remote call over the server to indicate that an error was recieved
                //Dont believe me take a look
                Context.ActorSelection(ActorConfig.RemoteAkkaServer).Tell(new ErrorMessage(string.Format("Were too hot captain our temperature is {0}", msg.Temperature),msg.ID));                
            });

            Receive<IMessageBase>(msg =>
            {
                //Lets apply some logic just for fun
                //we will analyse the entire message and pipe it to itself 
                //Whoa Akka talks to itself and its okay
                if (msg.GetSensorValues().ContainsKey(MessageTypeEnum.Temperature))
                {
                    Self.Tell(new TemperatureMessage(msg.DeviceID, msg.GetSensorValues()[MessageTypeEnum.Temperature]));
                };                

            });

        }    
    }
}
