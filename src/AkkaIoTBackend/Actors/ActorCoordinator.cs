﻿using Akka.Actor;
using AkkaIoTBackend.Messages;
using IoTCore;
using IoTCore.Messages;
using System;
using System.Collections.Generic;
using System.Text;

namespace AkkaIoTBackend.Actors
{
    //this will server as the entry point for the actor model
    //it will handle the requests as they come in and determine where they should go

    public class ActorCoordinator : ReceiveActor
    {

        public static string ActorName = "ActorCoordinator";

        private IActorRef _WeatherActor;        
        private IActorRef _navigationActor;
        private IActorRef _hardwareMonitorActor;
        private IActorRef _alertActor;
        private IActorRef _consoleActor;

        public ActorCoordinator()
        {
            Initalize();
        }


        private void Initalize()
        {
            Receive<IMessageBase>(x =>
            {
                if (x.DeviceID != null)
                {
                    DispatchActors(x);
                }
                return;
            });
        }

        protected override void PreStart()
        {
            base.PreStart();

            //create actors for usage here          
            _WeatherActor = Context.ActorOf(Props.Create(() => new WeatherActor()), WeatherActor.Name);          
            _navigationActor = Context.ActorOf(Props.Create(() => new NavigationActor()), NavigationActor.Name);
            _hardwareMonitorActor = Context.ActorOf(Props.Create(() => new HardwareActor()), HardwareActor.Name);
            _alertActor = Context.ActorOf(Props.Create(() => new AlertActor()), AlertActor.Name);
        }

        private bool AuthenticateMessage(IMessageBase msg)
        {
            if(!string.IsNullOrEmpty(msg.DeviceID) && msg.GetSensorValues() != null)
            {
                return true;
            }
            return false;
        }


        private void DispatchActors(IMessageBase msg)
        {   

            foreach (var sensor in msg.GetSensorValues())
            {
                //Makes an out of process call to a different process entirely:
                //Refer to the other console window to see all the readbacks that we are sending
                //Be mindful that the messages we send over the network need to be easily serializable to JSON or it will break
                //Keep messages small and easy to split up
                Context.ActorSelection(ActorConfig.RemoteAkkaServer).Tell(new SimpleDataMessage(msg.DeviceID,sensor.Key,sensor.Value));


                //These are local Actors they are still important but will be handled locally 
                switch (sensor.Key)
                {
                    case MessageTypeEnum.Temperature:         
                    case MessageTypeEnum.WindSpeed:
                        _WeatherActor.Tell(msg);
                        break;
                    case MessageTypeEnum.Direction:     
                    case MessageTypeEnum.GroudSpeed:
                    case MessageTypeEnum.PassengerCount:
                        _navigationActor.Tell(msg);
                        break;
                    case MessageTypeEnum.WheelPSI:
                    case MessageTypeEnum.EngineRPM:
                    case MessageTypeEnum.WingVibration:
                        _hardwareMonitorActor.Tell(msg);
                        break;
                }

            }
        }     

    }
}
