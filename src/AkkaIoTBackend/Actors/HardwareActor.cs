﻿using Akka.Actor;
using IoTCore;
using IoTCore.Messages;
using System;
using System.Collections.Generic;
using System.Text;

namespace AkkaIoTBackend.Actors
{
    public class HardwareActor : ReceiveActor
    {
        public const string Name = "HardwareActor";
        private IActorRef _fuelActor;

        private double _rpm;
        private double _vibration;

        public HardwareActor()
        {
            _fuelActor = Context.ActorOf(Props.Create(() => new FuelCalculatorActor()), FuelCalculatorActor.Name);


            Receive<IMessageBase>(msg => {

                var readbacks = msg.GetSensorValues();

                if(readbacks.ContainsKey(MessageTypeEnum.WheelPSI))
                {
                    var wheelPsi = readbacks[MessageTypeEnum.WheelPSI];
                    if(wheelPsi > 30)
                    {
                        Context.ActorSelection(ActorConfig.AlertActorConfig.Path).Tell(new LocalAlertMessage(msg.DeviceID, string.Format("Wheel PSI is over 30 reduce to normal operations current PSI: {0}", wheelPsi)));
                    }
                    else if(wheelPsi > 60)
                    {
                        Context.ActorSelection(ActorConfig.AlertActorConfig.Path).Tell(new GlobalAlertMessage(msg.DeviceID, string.Format("Wheel PSI is over 60 and wheel failure is gonna happen CurrnetPSI: {0}", wheelPsi)));
                    }
                }

                if(readbacks.ContainsKey(MessageTypeEnum.EngineRPM))
                {
                    _rpm = readbacks[MessageTypeEnum.EngineRPM];
                }
                if(readbacks.ContainsKey(MessageTypeEnum.WingVibration))
                {
                    _vibration = readbacks[MessageTypeEnum.WingVibration];
                }



                _fuelActor.Tell(new FuelMessage(msg.DeviceID, _vibration, _rpm));

            });
        }

    }
}
