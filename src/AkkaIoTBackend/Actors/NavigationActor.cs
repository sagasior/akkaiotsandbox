﻿using Akka.Actor;
using IoTCore;
using IoTCore.Messages;
using System;
using System.Collections.Generic;
using System.Text;

namespace AkkaIoTBackend.Actors
{

    internal class NavigationMessage
    {
        public string ID { get; set; }

        public MessageTypeEnum Type { get; set; }

        public double RawValue { get; set; }
    }

    internal class PassengerMessage
    {
        public string ID { get; set; }

        public MessageTypeEnum Type { get; set; }

        public double RawValue { get; set; }
    }


    public class NavigationActor : ReceiveActor
    {
        public const string Name = "NavigationActor";
                

        public NavigationActor()
        {
            Receive<IMessageBase>(msg =>
            {
                foreach(var sensor in msg.GetSensorValues())
                {
                   switch(sensor.Key)
                    {
                        case MessageTypeEnum.Direction:
                            Self.Tell(new NavigationMessage()
                            {
                                Type = sensor.Key,
                                RawValue = sensor.Value,
                                ID = msg.DeviceID
                            });
                            break;
                        case MessageTypeEnum.GroudSpeed:
                            Self.Tell(new NavigationMessage()
                            {
                                Type = sensor.Key,
                                RawValue = sensor.Value,
                                ID = msg.DeviceID
                            });
                            break;
                        case MessageTypeEnum.PassengerCount:
                            Self.Tell(new PassengerMessage()
                            {
                                Type = sensor.Key,
                                RawValue = sensor.Value,
                                ID = msg.DeviceID
                            });
                            break;
                    }                    

                }
            });


            Receive<NavigationMessage>(msg =>
            {
                //lets do some random prossesing

                if(msg.RawValue > 80)
                {
                    Context.ActorSelection(ActorConfig.ConsoleActorConfig.Path).Tell(new ConsoleWriteMessage(String.Format("Too Fast Dude your speed is {0} from ID {1}", msg.RawValue, msg.ID)));
                }                

            });

            Receive<PassengerMessage>(msg =>
            {
                //lets do some random prossesing
                if(msg.RawValue < 20)
                {
                    Context.ActorSelection(ActorConfig.RemoteAkkaServer).Tell(new ErrorMessage(String.Format("Why is the plane almost empty we only have {0} People",msg.RawValue),msg.ID));
                }            

            });

        }


    


    }
}
