﻿using Akka.Actor;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AkkaIoTBackend.Actors
{

    internal class FuelMessage
    {

        public FuelMessage(string id, double vibration, double rpm)
        {
            id = id;
            Vibration = vibration;
            RPM = rpm;
        }

        public string ID { get; private set; }
        public double Vibration { get; private set; }
        public double RPM { get;private set; }

    }

    internal class FuelLevel
    {

        public FuelLevel(double level)
        {
            Level = level;
        }

        public double Level { get; private set; }
    }

    internal class FuelWarning
    {
        public FuelWarning(double fuelLevel, bool shouldWarn)
        {
            FuelLevel = fuelLevel;
            ShouldWarn = shouldWarn;
        }

        public double FuelLevel { get; private set; }

        public bool ShouldWarn { get; private set; }
    }



    public class FuelCalculatorActor : ReceiveActor
    {
        public const string Name = "FuelCalculatorActor";

        private double _prevRPM;
        private double _prevVibration;

                

        public FuelCalculatorActor()
        {

            Receive<FuelMessage>(msg =>
            {

                if (Math.Abs(_prevRPM - msg.RPM) / _prevRPM > 0.5)
                {
                    Context.ActorSelection(ActorConfig.AlertActorConfig.Path).Tell(new LocalAlertMessage(msg.ID, String.Format("Too fast you rose from: {0} to {1}", _prevRPM, msg.RPM)));
                }

                if (Math.Abs(_prevVibration - msg.Vibration) / _prevVibration > 0.3)
                {       
                    Context.ActorSelection(ActorConfig.AlertActorConfig.Path).Tell(new LocalAlertMessage(msg.ID, String.Format("vibration too big it increased from: {0} to {1}", _prevVibration, msg.Vibration)));
                }  

                GetFuelLevel().ContinueWith(lvl =>
                {
                    var result = lvl.Result;

                    if (result.Level < 20)
                    {
                        return new FuelWarning(result.Level, true);
                    }

                    return new FuelWarning(result.Level, false);

                }).PipeTo(Self);               

            });

            Receive<FuelWarning>(msg => {
                if(msg.ShouldWarn)
                {
                    Context.ActorSelection(ActorConfig.AlertActorConfig.Path).Tell(new GlobalAlertMessage("000000",String.Format("Your fuel level is too low  Fuel Level: {0}", msg.FuelLevel)));
                }
            });   
                       
        }

        private async Task<FuelLevel> GetFuelLevel()
        {
            var rand = new Random();
            //simulate a long running operation
            for (int i = 0; i < 1000; i++)
            {
                rand.Next(0, 1000);
            }
            return new FuelLevel(rand.Next(0, 150));       

        }

    }
}
