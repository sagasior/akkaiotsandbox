﻿using Akka.Actor;
using System;
using System.Collections.Generic;
using System.Text;

namespace AkkaIoTBackend.Actors
{

    public class ConsoleWriteMessage
    {
        public ConsoleWriteMessage(string msg)
        {
            ConsoleMessage = msg;
        }

        public string ConsoleMessage { get; set; }
    }



    public class ConsoleActor : ReceiveActor
    {

        public static string Name = "consoleActor";

        public ConsoleActor()
        {

            Receive<ConsoleWriteMessage>(msg =>
            {
                Console.WriteLine(msg.ConsoleMessage);

            });            

        }       

    }
}
