﻿using Akka.Actor;
using IoTCore.Messages;
using System;
using System.Collections.Generic;
using System.Text;

namespace AkkaIoTBackend.Actors
{
    public class LocalAlertMessage
    {
        //only sends an alert to the local console
        public LocalAlertMessage(string id, string message)
        {
            ID = id;
            Message = message;
        }
                     
        public string ID { get; set; }

        public string Message { get; set; }

    }

    public class GlobalAlertMessage
    {
        public GlobalAlertMessage(string id, string message)
        {
            ID = id;
            Message = message;
        }

        //sends an alert to all consoles
        public string ID { get; set; }

        public string Message { get; set; }

    }


    public class AlertActor : ReceiveActor
    {
        
        public const string Name = "AlertActor";

        public AlertActor()
        {

            Receive<GlobalAlertMessage>(msg => {

                Context.ActorSelection(ActorConfig.RemoteAkkaServer).Tell(new ErrorMessage(String.Format("Sending a Global console Message: {0}", msg.Message), msg.ID));
                Context.ActorSelection(ActorConfig.ConsoleActorConfig.Path).Tell(new ConsoleWriteMessage(String.Format("Sending a Global console Message: {0}", msg.Message)));
            });


            Receive<LocalAlertMessage>(msg => {

                Context.ActorSelection(ActorConfig.ConsoleActorConfig.Path).Tell(new ConsoleWriteMessage(String.Format("Sending a local console Message: {0}", msg.Message)));
            });

        }

    }
}
