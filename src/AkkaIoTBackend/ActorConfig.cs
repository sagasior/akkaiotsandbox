﻿using AkkaIoTBackend.Actors;
using System;
using System.Collections.Generic;
using System.Text;

namespace AkkaIoTBackend
{

    public static class ActorConfig
    {
        //These are in process actors
        public static string ActorSystemName = "IoTSampleActorSystem";

        public static ActorData ConsoleActorConfig = new ActorData(ConsoleActor.Name, "akka://IoTSampleActorSystem/user");        

        public static ActorData AlertActorConfig = new ActorData(AlertActor.Name, "akka://IoTSampleActorSystem/user");

        //lets create an external process actor 
        //Just for fun ;)
        public static string RemoteAkkaServer = "akka.tcp://MyServer@localhost:8081/user/DataBaseServer";

    }          

    public class ActorData
    {
        public ActorData(string name, string parent)
        {
            Path = parent + "/" + name;
            Name = name;
        }

        public ActorData(string name)
        {
            Path = "akka://IoTSampleActorSystem/" + name;
            Name = name;
        }

        public string Name { get; private set; }

        public string Path { get; private set; }
    }
}
