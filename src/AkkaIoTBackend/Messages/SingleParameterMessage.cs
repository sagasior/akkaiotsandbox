﻿using IoTCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace AkkaIoTBackend.Messages
{
    public class SingleParameterMessage
    {
        public MessageTypeEnum MessageType { get; set; }

        public double value { get; set; }

        public string DeviceID { get; set; }

        public SingleParameterMessage(string device, MessageTypeEnum type, double rawValue)
        {
            MessageType = type;
            value = rawValue;
            DeviceID = device;
        }
    }
}
