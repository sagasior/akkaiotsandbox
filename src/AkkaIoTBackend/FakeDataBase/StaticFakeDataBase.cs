﻿using IoTCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace AkkaIoTBackend.FakeDataBase
{
    public static class StaticFakeDataBase
    {
        
        public static void WriteToDataBase(string deviceID, MessageTypeEnum sensorType, double Value)
        {
            //This is a fake database call that we will assume takes 5ms to complete    
            //For a few $$'s I may implement it for you :)
            Thread.Sleep(5);
            return;
        }

        public static void LongQuerySimulate(int msDelay = 50)
        {
            Thread.Sleep(msDelay);
        }

    }
}
