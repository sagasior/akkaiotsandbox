﻿using Akka.Actor;
using Akka.Configuration;
using AkkaIoTBackend.Actors;
using IoTCore.Messages;
using System;
using System.Collections.Generic;
using System.Text;
using static Akka.IO.Tcp;

namespace AkkaIoTBackend
{

    public class LoadBalancer
    {
        private ActorSystem _actorSystem;
        IActorRef _controllerActor;

        Akka.Configuration.Config config = ConfigurationFactory.ParseString(@"
akka {  
    actor {
        provider = ""Akka.Remote.RemoteActorRefProvider, Akka.Remote""
    }
    remote {
        dot-netty.tcp {
		    port = 0
		    hostname = localhost
        }
    }
}
");
        public LoadBalancer()
        {  
            _actorSystem = ActorSystem.Create(ActorConfig.ActorSystemName, config);
            _controllerActor = _actorSystem.ActorOf(Props.Create(() => new ActorCoordinator()), ActorCoordinator.ActorName);
            _actorSystem.ActorOf(Props.Create<ConsoleActor>(), ActorConfig.ConsoleActorConfig.Name);
            _actorSystem.ActorOf(Props.Create<AlertActor>(), ActorConfig.AlertActorConfig.Name);
        }        

        public void HandleMessage(IMessageBase msg)
        {
            _controllerActor.Tell(msg);
        }
        //testCode
        
      
    }
}
