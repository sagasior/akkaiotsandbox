﻿using IoTCore;
using IoTCore.Messages;
using System;
using System.Collections.Generic;
using System.Text;

namespace AkkaDemoIoT
{
    public class SampleMessage : IMessageBase
    {

        private string _deviceID;

        Dictionary<MessageTypeEnum, double> _sensorData;

        public SampleMessage(string deviceID)
        {
            _sensorData = new Dictionary<MessageTypeEnum, double>();
            _deviceID = deviceID;
        }

        public string DeviceID
        {
            get
            {
                return _deviceID;
            }
        }

        public void AddSensorPacket(MessageTypeEnum type, double value)
        {          
            _sensorData[type] = value;           
        }

        public Dictionary<MessageTypeEnum, double> GetSensorValues()
        {
            return _sensorData;
        }
    }
}
