﻿using AkkaIoTBackend;
using IoTCore;
using IoTCore.Messages;
using System;

namespace AkkaDemoIoT
{
    class Program
    {
        static void Main(string[] args)
        {

            var srvr = new LoadBalancer();

            Console.WriteLine("Press Enter To Send A New Random Data Package");
            Console.WriteLine("Or type exit to quit the application");

            while(true)
            {

                var tmpMsg = CreateRandomMessage();
                                
                srvr.HandleMessage(tmpMsg);

                var tmp = Console.ReadLine();
                if (tmp == "exit")
                    return;

            }                      
           
        }
        
        static IMessageBase CreateRandomMessage()
        {
            var tmpMsg = new SampleMessage(String.Concat(DateTime.Now.Ticks, "-StringRandom"));

            var randNumber = new Random();
            var numSensorData = randNumber.Next(1, 10);
            var randEnum = typeof(MessageTypeEnum).GetEnumValues();

            for (int i = 0; i < numSensorData; i++)
            {
                tmpMsg.AddSensorPacket((MessageTypeEnum)randEnum.GetValue(randNumber.Next(0, randEnum.Length)), randNumber.Next(0, 100));
            }
            return tmpMsg;
        }

    }
}
