﻿using Akka.Actor;
using Akka.Configuration;
using System;

namespace AkkaSecondServerConsole
{
    class Program
    {

        static Config config = ConfigurationFactory.ParseString(@"
akka {  
    actor {
        provider = ""Akka.Remote.RemoteActorRefProvider, Akka.Remote""
    }
    remote {
        dot-netty.tcp {
            port = 8081
            hostname = 0.0.0.0
            public-hostname = localhost
        }
    }
}
");


        static void Main(string[] args)
        {

            Console.WriteLine("Welcome to the server side :)");

            using (var system = ActorSystem.Create("MyServer", config))
            {

                //path will be 
                //"akka.tcp://MyServer@localhost:8081/user/DataBaseServer"
                system.ActorOf<ServerDatabaseActor>("DataBaseServer");

                Console.ReadLine();
            }            
        }
    }
}
