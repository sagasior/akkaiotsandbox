﻿using Akka.Actor;
using IoTCore.Messages;
using System;
using System.Collections.Generic;
using System.Text;

namespace AkkaSecondServerConsole
{
    public class ServerDatabaseActor : ReceiveActor
    {

        public ServerDatabaseActor()
        {
            Receive<SimpleDataMessage>(msg =>
            {   
                Console.WriteLine(String.Format("Sensor Type: {0}    Value: {1}     ID: {2}", msg.SesnorType.ToString(), msg.RawValue, msg.DeviceID));                
            });


            Receive<ErrorMessage>(msg =>
            {
                Console.Write(String.Format("!!!!!!!!!!!!!!!!!IoT Error Recieved from ", msg.MessageID));

                Console.WriteLine(msg.DisplayString);

                Console.WriteLine();
            });
        }

    }
}
